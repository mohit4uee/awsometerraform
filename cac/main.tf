# terraform main script
terraform {
  backend "gcs" {
     bucket = "tf-state-storage1234"
     prefix = "awsome/cac"
     credentials = "tf-state-key.json"
     project = "kube-cluster-228807"
     region =  "europe_west2"
    }
}
# configure the Google Cloud provider
 provider "google" {
   version =  "1.18.0"
   project =  "${var.project_id}"
   region =  "${var.region}"
   zone = "${var.project_zone}"
   credentials =  "${var.tf_kube_admin_key}"
}


resource "google_service_account" "gke_sa" {
  account_id   = "${var.gke-service-account}"
  display_name = "Terraform managed"
}
/*
data "google_iam_policy" "sa_user" {
  binding {
    role = "${var.gke_service_account_role}"

    members = [
      "serviceAccount:${google_service_account.gke_sa.email}",
    ]
  }
}

resource "google_service_account_iam_policy" "gke_account_iam" {
  service_account_id = "${google_service_account.gke_sa.id}"
  policy_data        = "${data.google_iam_policy.sa_user.policy_data}"

}

*/


