variable "project_name" {
  description = "The human readable name for the project"
  default = "kube-cluster"
}
variable "project_id" {
  description = "The unique id for the project"
  default ="kube-cluster-228807"
}

variable "tf_kube_admin_key" {
  description = "The SA key for the Terraform Admin account for the target project"
  default = "tf_kube_admin_key"
}
variable "gke-service-account" {
  description = "The SA key for the Terraform Admin account for the target project"
  default = "gke-service-account"
}

variable "region" {
  description = "The default region for regional project resources"
  default     = "europe-west2"
}
variable "zone" {
  description = "The default zone for zonal project resources"
  default     = ["europe-west2-b", "europe-west2-a" ]
}
variable "project_zone" {
  description = "The default zone for zonal project resources"
  default     = "europe-west2-b"
}

variable "gke_service_account_role" {
  description = "The default zone for zonal project resources"
  default     = "roles/Editor"
}


// General Variables (For Kubernates, added by Mohit)
variable "linux_admin_username" {
    type        = "string"
    description = "User name for authentication to the Kubernetes linux agent virtual machines in the cluster."
    default = "mohit"
}
variable "linux_admin_password" {
    type ="string"
    description = "The password for the Linux admin account."
    default = "password12345678"
}
// GCP Variables (For Kubernates, added by Mohit)
variable "gcp_cluster_count" {
    type = "string"
    description = "Count of cluster instances to start."
    default = "1"
}
variable "cluster_name" {
    type = "string"
    description = "Cluster name for the GCP Cluster."
    default  = "krb-cluster"
}
