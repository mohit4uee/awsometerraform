# remote state backend on GCS
terraform {
  backend "gcs" {
    bucket      = "tf-state-storage1234"
    prefix      = "awsome/project"
    credentials = "tf-state-key.json"
    project     = "BrandNew"
    region      =  "europe_west2"
  }
}

# configure the Google Cloud provider
provider "google" {
  version     = "1.18.0"
  credentials = "${var.project_creator_key}"
}

# create a unique project id, following our convention: this-that-number
resource "random_integer" "project_id_suffix" {
  min  = 100000
  max  = 999999
  seed = "${uuid()}"

  lifecycle {
    ignore_changes = ["seed"]
  }
}

resource "random_pet" "project_id_pieces" {
  length    = "2"
  separator = "-"
}

# create a GCP project
resource "google_project" "project" {
  name            = "${var.project_name}"
  project_id      = "${format("%s-%s",random_pet.project_id_pieces.id, random_integer.project_id_suffix.result)}"
  billing_account = "${var.billing_account_id}"
  folder_id       = "${var.project_folder_id}"
}

# enable services in the GCP project
resource "google_project_services" "enable_project_apis" {
  project    = "${google_project.project.project_id}"
  services   = "${var.project_apis}"
  depends_on = ["google_project.project"]
}

# create the terraform admin account for use when provisioning the project resources
resource "google_service_account" "terraform_admin" {
  account_id   = "${var.terraform_account_name}"
  display_name = "Terraform Admin"
  project      = "${google_project.project.project_id}"
}

# grant a role to the terraform admin account
resource "google_project_iam_member" "terraform_editor" {
  project = "${google_project.project.project_id}"
  role    = "roles/editor"
  member  = "serviceAccount:${google_service_account.terraform_admin.email}"
}

# enabled os-login
resource "google_compute_project_metadata_item" "oslogin" {
  project    = "${google_project.project.project_id}"
  key        = "enable-oslogin"
  value      = "TRUE"
  depends_on = ["google_project_services.enable_project_apis"]
}


