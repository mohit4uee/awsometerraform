variable "project_id" {
  description = "The unique id for the project"
  default ="ornate-opus-226104"
}
variable "project_name" {
  description = "The human readable name for the project"
  default = "My First Project"
}

variable "terraform_admin_key" {
  description = "The SA key for the Terraform Admin account for the target project"
}

variable "region" {
  description = "The default region for regional project resources"
  default     = "europe-west2"
}
variable "zone" {
  description = "The default zone for zonal project resources"
  default     = "europe-west2-b"
}

variable "shared_vpc_project_id" {
  description = "The id of the shared vpc host project"
  default     = "ornate-opus-226104"
}

variable no_external_ip_access_config {
  description = "The access config block on instance for which we want no External IP"
  type        = "list"

  // set to [] to remove external IP
  default = []
}
variable "compute_sa_name" {
  description = "The name of the Terraform admin account to be created in the project"
  default     = "project-compute-user"
}


